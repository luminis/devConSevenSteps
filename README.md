#hazel#

A small project to demonstrate a Continuous Delivery pipeline.
Written in Java, using Maven as the build management tool.

It consists of 4 submodules:

* Infra
The infrastructe configuration based on Docker
* Bookservicestub
A (backend) service used by the webshop. It has a REST interface
* Webshop
The frontend.
* Integration
Integration tests.
  
Next to these there is the _jenkins_ directory. This is a backup of the Jenkins configurations.

##Prerequisites##
Make sure Sonar is up and running and properly configured in Jenkins
If you run this on OSX make sure boot2docker is started :
```
boot2docker up
$(boot2docker shellinit)
```
Add to the jenkins startup script: 
```export DOCKER_OPTS=' --host=unix:///var/run/docker.sock --restart=false  -G jenkins' ```
If you run this on OSX: Jenkins should be started in the same terminal as you have started boot2docker.

###Testing###
Testing can be done on 3 levels:

* Unit
By default run by `mvn clean install` from the root of the project.
* Component
Tests a specific component. Run by `mvn clean verify -P componenttest` from the root of the project.
* Integration
Tests the integration of webshop and bookservice. Run by `mvn clean verify -P integrationtest` from the root of the project.
  
###Continuous Delivery pipeline###

1. Build
2. Unit tests
3. Component tests
4. Quality
5. Package
Packages both projects into 2 separate war files
6. Integration tests
Runs the integration tests, by:

  1. Copies `bookservicestub/target/*.war` to `infra/bookservice/` and `webshop/target/*.war` to `infra/webshop/`
  2. ```
    cd infra/bookservice
    docker build -t herbert/bookservice .
    docker run -d -p 18080:8080 --name bookservice herbert/bookservice
     ```
  3. ```
    cd infra/webshop
    docker build -t herbert/webshop .
    docker run -d -p 28080:8080 --name webshop herbert/webshop
    ```
  4. Run the integration test: `mvn clean verify -P integrationtest`
  5. Stop Docker by 
    ```
    docker stop webshop bookservice
    docker rm webshop bookservice
    ```
For simplicity reasons the integration test is he same as the webshop component test. Different data though.