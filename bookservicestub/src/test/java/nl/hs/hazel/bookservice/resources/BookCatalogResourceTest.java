package nl.hs.hazel.bookservice.resources;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.matcher.Matchers;
import nl.hs.hazel.bookservice.domain.Book;
import nl.hs.hazel.bookservice.services.CatalogService;
import nl.hs.hazel.bookservice.utils.slf4jlogger.Slf4JTypeListener;
import org.jboss.resteasy.core.Dispatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.*;
import static org.mockito.Matchers.contains;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class BookCatalogResourceTest {

    @Inject
    private BookCatalogResource resource;

    @Parameterized.Parameters(name = "{index}: class = {0}, school = {1}, stad = {2}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"Leerjaar 1 nivo 4", "Johan Cruyff College", "Nijmegen"},
                {"Sociaal Cultureel Werker 20+ leerjaar 2", "ID College Welzijn", "Leiden"},
                {"ICT beheer niv 4 lj 3 BOL", "ROC A12 ICT", "Arnhem"}
        });
    }

    @Parameterized.Parameter(value = 0)
    public String classLevel;
    @Parameterized.Parameter(value = 1)
    public String school;
    @Parameterized.Parameter(value = 2)
    public String stad;

    private Dispatcher dispatcher;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(getTestModule()).injectMembers(this);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetMsg() throws Exception {
        Response response = resource.getBooksForCitySchoolAndClass(stad, school, classLevel);

        assertThat(response.getStatus(), is(HttpServletResponse.SC_OK));
        Book book = (Book)((ArrayList)response.getEntity()).get(0);
        assertThat(book.getTitle(), containsString(school));
    }

    private Module getTestModule() {
        return new AbstractModule() {
            @Override
            protected void configure() {
                CatalogService cs = mock(CatalogService.class);
                when(cs.getCatalog(school, classLevel, stad)).thenReturn(generateResponse());

                bind(BookCatalogResource.class);
                bind(CatalogService.class).toInstance(cs);
                bindListener(Matchers.any(), new Slf4JTypeListener());
            }

            private List<Book> generateResponse() {
                List<Book> books = new ArrayList<Book>();
                Book b1 = new Book();
                b1.setTitle("title:" + school);
                books.add(b1);
                return books;
            }
        };
    }
}