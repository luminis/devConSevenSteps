package nl.hs.hazel.bookservice.resources;

import com.google.inject.Inject;
import nl.hs.hazel.bookservice.domain.Book;
import nl.hs.hazel.bookservice.services.CatalogService;
import nl.hs.hazel.bookservice.utils.slf4jlogger.InjectLogger;
import org.slf4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/books")
public class BookCatalogResource {

    @InjectLogger
    Logger logger;
    @Inject
    private CatalogService catalogService;

    @GET
    @Path("/{city}/{school}/{classLevel}")
    @Produces("application/json")
    public Response getBooksForCitySchoolAndClass(@PathParam("city") String city,
                             @PathParam("school") String school,
                             @PathParam("classLevel") String classLevel) {
        logger.info("Books:: city: "+city+", school: "+school+", class: "+classLevel+"<<<");

        List<Book> catalog = catalogService.getCatalog(school, classLevel, city);
        return Response.status(200).entity(catalog).build();
    }
}
