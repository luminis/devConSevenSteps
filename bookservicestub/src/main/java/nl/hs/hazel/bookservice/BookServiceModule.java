package nl.hs.hazel.bookservice;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;
import com.google.inject.name.Names;
import nl.hs.hazel.bookservice.resources.BookCatalogResource;
import nl.hs.hazel.bookservice.services.CatalogService;
import nl.hs.hazel.bookservice.utils.slf4jlogger.Slf4JTypeListener;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class BookServiceModule extends AbstractModule {
    @Override
    protected void configure() {
        readProperties();
        bind(BookCatalogResource.class);
        bind(CatalogService.class);
        bindListener(Matchers.any(), new Slf4JTypeListener());
    }

    private void readProperties() {
        Properties properties = new Properties();
//        String env = System.getProperty("env");
        String confDir = System.getenv("BS_CONF_PATH");
        if(confDir == null) {
            confDir = System.getProperty("BS_CONF_PATH");
        }

        try {
            properties.load(new FileReader(confDir + File.separator + "bookcatalog.properties"));
            System.out.println("props: " + properties);
            Names.bindProperties(binder(), properties);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
