package nl.hs.hazel.bookservice.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import nl.hs.hazel.bookservice.domain.Book;
import nl.hs.hazel.bookservice.utils.slf4jlogger.InjectLogger;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class CatalogService {

    @InjectLogger
    Logger logger;

    private String catalogPath;

    @Inject
    public CatalogService(@Named("catalog.path") String catalogPath) {
        this.catalogPath = catalogPath;
    }

    public List<Book> getCatalog(String school, String classLevel, String city) {
        String resourceName = constructResourceName(school, classLevel, city);
        logger.info("resourcepath: " + resourceName);

        File file = new File(resourceName);
        logger.info("resource file: " + file);

        ObjectMapper mapper = new ObjectMapper();
        try {
            List<Book> books = mapper.readValue(file, new TypeReference<List<Book>>(){});
            logger.info("book: "+books+"<<<<<");
            return books;
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("returning empty list");
        return new ArrayList();
    }

    private String constructResourceName(String school, String classLevel, String city) {
        StringBuffer sb = new StringBuffer();
        sb.append(catalogPath);
        sb.append(File.separator);
        sb.append(city);
        sb.append(File.separator);
        sb.append(school);
        sb.append(File.separator);
        sb.append(classLevel);
        sb.append(File.separator);
        sb.append("catalog.json");
        return sb.toString();
    }
}
