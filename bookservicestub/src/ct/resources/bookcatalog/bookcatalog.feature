Feature:
  As a user of the catalog service
  I want a catalog of all books for my city, school and class
  So that I can easily select all necessary books

  Scenario: Show the list of books
    Given my class is '3a' of the 'Padua' in 'Tilburg'
    When I request for the catalog
    Then I get a list of all books
