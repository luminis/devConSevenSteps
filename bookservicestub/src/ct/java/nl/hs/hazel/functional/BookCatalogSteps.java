package nl.hs.hazel.functional;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BookCatalogSteps {

    private String classLevel;
    private String school;
    private String city;
    private WebTarget target;
    private Response response;

    @Given("^my class is \'(.+)\' of the \'(.+)\' in \'(.+)\'")
    public void classSchoolCity(String classLevel, String school, String city) throws Throwable {
        this.classLevel = classLevel;
        this.school = school;
        this.city = city;

        Client client = ClientBuilder.newClient();
        target = client.target(getBaseURI());
    }

    @When("^I request for the catalog")
    public void IRequestTheListOfBooks() throws Throwable {
        response = target.path("books").path(city).path(school).path(classLevel).request()
                .accept(MediaType.APPLICATION_JSON).get();
    }

    @Then("^I get a list of all books")
    public void IGetAListOfAllBooksINeedForMyClass() throws Throwable {
        assertThat(response.getStatus(), is(200));
        assertThat(response.readEntity(String.class), is("[{\"title\":\"Van Dale pocketwoordenboek duits-nederlands\",\"author\":\"Van Dale\",\"price\":10.99,\"isbn\":\"9789460770579\"},{\"title\":\"Het groene boekje\",\"author\":\"SDU\",\"price\":27.95,\"isbn\":\"9789012105903\"}]"));
        response.close();
    }

    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost:9090/bookcatalog").build();
    }
}