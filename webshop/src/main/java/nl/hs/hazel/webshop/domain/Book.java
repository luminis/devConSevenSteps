package nl.hs.hazel.webshop.domain;

import lombok.Data;

@Data
public class Book {

    private String title;
    private String author;
    private Double price;
    private String isbn;
}
