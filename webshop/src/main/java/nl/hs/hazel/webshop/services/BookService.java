package nl.hs.hazel.webshop.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import nl.hs.hazel.webshop.domain.Book;
import nl.hs.hazel.webshop.utils.slf4jlogger.InjectLogger;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.slf4j.Logger;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class BookService implements IBookService {

    @InjectLogger Logger logger;
    private WebTarget target;

    @Inject
    public BookService( @Named("bookservice.host") String host) {
        ResteasyClientBuilder client = new ResteasyClientBuilder();
        client = client.connectionPoolSize(20);
        target = client.build().target(UriBuilder.fromUri(host).build());
    }

    public List<Book> getBooks(String school, String classLevel, String city) {
        Response response = target.path("books").path(city).path(school).path(classLevel).request()
                .accept(MediaType.APPLICATION_JSON).get();
        logger.debug("getBooks status: "+response.getStatus()+"");
        logger.debug("getBooks entity: "+response.getEntity());


        if(response.getStatus() == Response.Status.OK.getStatusCode()) {
            List<Book> books = response.readEntity(new GenericType<List<Book>>(){});
            logger.debug("Response bookservice: "+books+"<<<<<");
            return books;
        } else {
          return new ArrayList();
        }
    }
}
