package nl.hs.hazel.webshop.services;

import nl.hs.hazel.webshop.domain.Book;

import java.util.List;

public interface IBookService {
    public List<Book> getBooks(String school, String classLevel, String city);
}
