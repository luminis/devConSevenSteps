package nl.hs.hazel.webshop.resources;

import com.google.inject.Inject;
import nl.hs.hazel.webshop.domain.Book;
import nl.hs.hazel.webshop.services.IBookService;
import nl.hs.hazel.webshop.utils.slf4jlogger.InjectLogger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.List;
import org.slf4j.Logger;

@Path("/books")
public class BookOverviewResource {

    @InjectLogger Logger logger;
    @Inject private IBookService bookService;

    @GET
    @Path("/{city}/{school}/{classLevel}")
    @Produces("application/json")
    public Response getBooks(@PathParam("city") String city,
                           @PathParam("school") String school,
                           @PathParam("classLevel") String classLevel) {
        List<Book> books = bookService.getBooks(school, classLevel, city);
        // todo return book list
        //Response.status(200).entity(books).build();
        return Response.status(200).entity(school).build();
    }

}
