package nl.hs.hazel.webshop.utils.slf4jlogger;

import com.google.inject.MembersInjector;

import java.lang.reflect.Field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Slf4JMembersInjector<T> implements MembersInjector<T> {
    private final Field field;
    private final Logger logger;

    Slf4JMembersInjector(Field field) {
        this.field = field;
        this.logger = LoggerFactory.getLogger(String.valueOf(field.getDeclaringClass()));
        field.setAccessible(true);
    }

    public void injectMembers(T t) {
        try {
            field.set(t, logger);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
