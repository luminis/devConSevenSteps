package nl.hs.hazel.webshop;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;
import com.google.inject.name.Names;
import nl.hs.hazel.webshop.resources.BookOverviewResource;
import nl.hs.hazel.webshop.services.BookService;
import nl.hs.hazel.webshop.services.IBookService;
import nl.hs.hazel.webshop.utils.slf4jlogger.Slf4JTypeListener;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class WebshopModule extends AbstractModule {

    @Override
    protected void configure() {
        readProperties();
        bind(BookOverviewResource.class);
        bind(IBookService.class).to(BookService.class);
        bindListener(Matchers.any(), new Slf4JTypeListener());
    }

    private void readProperties() {
        Properties properties = new Properties();
//        String env = System.getProperty("env");
        String confDir = System.getenv("WS_CONF_PATH");
        if(confDir == null) {
            confDir = System.getProperty("WS_CONF_PATH");
        }

        try {
            properties.load(new FileReader(confDir + File.separator + "webshop.properties"));
            System.out.println("props: " + properties);
            Names.bindProperties(binder(), properties);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
