package nl.hs.hazel.webshop.resources;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.matcher.Matchers;
import nl.hs.hazel.webshop.services.BookService;
import nl.hs.hazel.webshop.services.IBookService;
import nl.hs.hazel.webshop.utils.slf4jlogger.Slf4JTypeListener;
import org.jboss.resteasy.core.Dispatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import java.util.Arrays;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

@RunWith(Parameterized.class)
public class BookOverviewResourceTest {

    @Inject private BookOverviewResource resource;

    @Parameterized.Parameters(name = "{index}: class = {0}, school = {1}, stad = {2}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"class", "school", "stad"}, {"class2", "school2", "stad2"}, {"clas3", "school3", "stad3"}
        });
    }

    @Parameterized.Parameter(value = 0)
    public String classLevel;
    @Parameterized.Parameter(value = 1)
    public String school;
    @Parameterized.Parameter(value = 2)
    public String stad;

    private Dispatcher dispatcher;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(getTestModule()).injectMembers(this);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetMsg() throws Exception {
        Response response = resource.getBooks(stad, school, classLevel);

        assertThat(response.getStatus(), is(HttpServletResponse.SC_OK));
        assertThat(response.getEntity().toString(), is(school));
    }

    private Module getTestModule() {
        return new AbstractModule() {
            @Override
            protected void configure() {
                BookService bs = mock(BookService.class);
                bind(BookOverviewResource.class);
                bind(IBookService.class).toInstance(bs);
                bindListener(Matchers.any(), new Slf4JTypeListener());
            }
        };
    }
}