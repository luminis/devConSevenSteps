Feature:
  As a customer of the webshop
  I want an overview of all books I have to order for my lessons
  So that I can easily select all necessary books

  Scenario: Show the list of books
    Given my class is 'havo 3' of the 'Padua' in 'Tilburg'
    When I request for the list of books
    Then I get a list of all books I need for my class


#  Scenario: Retrieve book details
#    Given a list of school books
#    When I select 'Sportmarketing'
#    Then the title, price, isbn and description of the book should be displayed
