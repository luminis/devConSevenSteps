package nl.hs.hazel.functional;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.mockserver.client.server.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.matchers.Times;
import org.mockserver.model.Cookie;
import org.mockserver.model.Delay;
import org.mockserver.model.Header;
import org.mockserver.model.Parameter;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public class BookOverviewSteps {

    private ClientAndServer mockServer;

    private String classLevel;
    private String school;
    private String city;
    private WebTarget target;
    private Response response;

    @Given("^my class is \'(.+)\' of the \'(.+)\' in \'(.+)\'")
    public void classSchoolCity(String classLevel, String school, String city) throws Throwable {
        this.classLevel = classLevel;
        this.school = school;
        this.city = city;

        startMockBookService();

        Client client = ClientBuilder.newClient();
        target = client.target(getBaseURI());
    }

    @When("^I request for the list of books")
    public void IRequestTheListOfBooks() throws Throwable {
        response = target.path("books").path(city).path(school).path(classLevel).request()
                .accept(MediaType.APPLICATION_JSON).get();
    }

    @Then("^I get a list of all books I need for my class")
    public void IGetAListOfAllBooksINeedForMyClass() throws Throwable {
        assertThat(200, is(response.getStatus()));
        assertThat(school, is(response.readEntity(String.class)));
        response.close();

//        NettyContainer.stop();
    }

    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost:8080/webshop").build();
    }

    private void startMockBookService() {
        mockServer = startClientAndServer(1234);
        mockServer
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/bookservice/books/Tilburg/Padua/havo%203"),
                        Times.exactly(1)
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeaders(
                                        new Header("Content-Type", "application/json; charset=utf-8"),
                                        new Header("Cache-Control", "public, max-age=86400"),
                                        new Header("HS-TEST", "hello")
                                )
                                .withBody("[{\n" +
                                        "    \"title\":\"Van Dale pocketwoordenboek duits-nederlands\",\n" +
                                        "    \"author\":\"Van Dale\",\n" +
                                        "    \"price\": 10.99,\n" +
                                        "    \"isbn\":\"9789460770579\"\n" +
                                        "},\n" +
                                        "{\n" +
                                        "    \"title\":\"Het groene boekje\",\n" +
                                        "    \"author\":\"SDU\",\n" +
                                        "    \"price\":27.95,\n" +
                                        "    \"isbn\":\"9789012105903\"\n" +
                                        "}]")
                                .withDelay(new Delay(TimeUnit.SECONDS, 1))
                );

    }
}