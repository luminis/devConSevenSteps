package nl.hs.hazel.integration;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BookOverviewSteps {

    private String classLevel;
    private String school;
    private String city;
    private WebTarget target;
    private Response response;

    @Given("^my class is \'(.+)\' of the \'(.+)\' in \'(.+)\'")
    public void classSchoolCity(String classLevel, String school, String city) throws Throwable {
        this.classLevel = classLevel;
        this.school = school;
        this.city = city;

        Client client = ClientBuilder.newClient();
        target = client.target(getBaseURI());
    }

    @When("^I request for the list of books")
    public void IRequestTheListOfBooks() throws Throwable {
        response = target.path("books").path(city).path(school).path(classLevel).request()
                .accept(MediaType.APPLICATION_JSON).get();
    }

    @Then("^I get a list of all books I need for my class")
    public void IGetAListOfAllBooksINeedForMyClass() throws Throwable {
        assertThat(200, is(response.getStatus()));
        assertThat(school, is(response.readEntity(String.class)));
    }

    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://192.168.59.103:28080/webshop").build();
    }
}