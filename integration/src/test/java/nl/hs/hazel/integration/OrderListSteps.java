package nl.hs.hazel.integration;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class OrderListSteps {

    private String userName;
    private WebTarget target;
    private Response response;

    @Given("^the user \'(.+)\'")
    public void the_user(String user) throws Throwable {
        this.userName = user;

        Client client = ClientBuilder.newClient();
        target = client.target(getBaseURI());
    }

    @When("^I request for the list of orders")
    public void I_get_the_list_of_orders() throws Throwable {
        response = target.path("rest").path("orders").path(userName).request()
                .accept(MediaType.TEXT_PLAIN_TYPE).get();
    }

    @Then("^I get the list belonging to that user")
    public void I_get_the_list_belonging_to_that_user() throws Throwable {
        assertThat(200, is(response.getStatus()));
        assertThat(userName, is(response.readEntity(String.class)));
    }

    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost:8080/webshop").build();
    }
}