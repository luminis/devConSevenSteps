Feature:
      As a support member of the webshop
  	  I want an overview of all orders from a user
  	  So that I can easily help the user if he calls

      @ignore
  	  Scenario: Showing the list of orders
  	    Given the user 'piet'
  	    When I request for the list of orders
  	    Then I get the list belonging to that user